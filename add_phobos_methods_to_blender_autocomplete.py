import sys, os

sys.path.append("../phobos")
sys.path.append("../blender_autocomplete/3.0")

from phobos.operators.editing import *
from phobos.operators.naming import *

import inspect

PARENT_DIRECTORY = os.path.dirname(os.path.dirname(__file__))
REALTIVE_PATH = "blender_autocomplete/3.0/bpy/ops/phobos.py"
FILE_PATH = os.path.join(PARENT_DIRECTORY, REALTIVE_PATH)


def print_def(method_name, arguments_total, docstring, params, file_to_write_to):
    file_to_write_to.write("\n")
    file_to_write_to.write(
        f"def {method_name}({arguments_total}):{docstring}{params} \n\t'''"
    )


def main():
    file_to_write_to = open(FILE_PATH, "w")
    operators = inspect.getmembers(sys.modules[__name__], inspect.isclass)

    for i in range(len(operators)):
        operator = operators[i]
        operator_class = globals()[operator[0]]

        if hasattr(operator_class, "bl_idname"):
            name = operator_class.bl_idname
            if name is not None:

                method_name = name.replace("phobos.", "")
                docstring = "\n\t'''" + operator_class.__doc__ + "\n"
                arguments = list(operator_class.__annotations__.keys())

                if arguments[0] != "bl_cursor_pending":

                    values = []
                    docs = []

                    for arg in arguments:
                        value = operator_class.__annotations__[arg]["default"]
                        doc = operator_class.__annotations__[arg]["description"]
                        docs.append(doc)
                        if isinstance(value, str):
                            values.append("'" + value + "'")
                        else:
                            values.append(value)

                    arguments_total = ""

                    params = ""

                    for j in range(len(arguments)):
                        arg_type = values[j].__class__.__name__
                        if arg_type == "NoneType":
                            arg_type = "None"

                        arguments_total += (
                            arguments[j] + ": " + arg_type + " = " + str(values[j])
                        )

                        param_docstring = "\n\t:param "
                        param_type = "\n\t:type "

                        param_docstring += arguments[j] + ": " + str(docs[j])
                        param_type += arguments[j] + ": " + arg_type
                        params += param_docstring + param_type

                        if j < len(arguments) - 1:
                            arguments_total += ", "

                    print_def(
                        method_name,
                        arguments_total,
                        docstring,
                        params,
                        file_to_write_to,
                    )
                    print(name)
    file_to_write_to.close()
    print("done")


if __name__ == "__main__":
    main()
