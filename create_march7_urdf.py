from typing import List
import bpy
import numpy as np
from mathutils import Matrix
import os

DIRNAME = os.path.dirname(__file__)

# Classes:


class Dimensions:
    def __init__(self, x: float = 0.1, y: float = 0.1, z: float = 0.1):
        self.x = x
        self.y = y
        self.z = z

    @property
    def as_list(self):
        return [self.x, self.y, self.z]


class Location:
    def __init__(self, x: float = 0.0, y: float = 0.0, z: float = 0.0):
        self.x = x
        self.y = y
        self.z = z

    @property
    def as_list(self):
        return [self.x, self.y, self.z]


class Limit:
    def __init__(self, upper, lower) -> None:
        self.upper = upper
        self.lower = lower


class Link:
    def __init__(
        self,
        name: str,
        parent: "Link",
        location: Location,
        dimensions: Dimensions,
        mass: float = 2,
        ax_rot: str = None,
        ax_link: str = None,
        limit: Limit = None,
    ):
        self.name = name
        self.parent = parent
        self.loc = location
        self.dim = dimensions
        self.mass = mass
        self.ax_rot = ax_rot
        self.ax_link = ax_link
        self.lim = limit

    @property
    def loc_world(self):
        if self.parent is None:
            return np.array(self.loc.as_list)
        else:
            return self.parent.loc_world + np.array(self.loc.as_list)


# Definitions:
PI = np.pi
NAME = "march7"

LEN_HIP_BASE = 0.132
LEN_HIP_AA_SIDE = 0.234
LEN_HIP_AA_FRONT = 0.189
LEN_UPPER_LEG = 0.41
DISPLACE_UPPER_LEG = 0.07
LEN_LOWER_LEG = 0.397
DISPLACE_LOWER_LEG = 0.01
LEN_FOOT = 0.35

HEIGHT_FOOT = 0.1
Z_HIP_BASE = LEN_UPPER_LEG + LEN_LOWER_LEG + HEIGHT_FOOT

LIM = Limit(upper=2, lower=0)


# Links:
hip_base = Link(
    name="hip_base",
    parent=None,
    location=Location(z=Z_HIP_BASE),
    dimensions=Dimensions(y=LEN_HIP_BASE),
)
left_hip = Link(
    name="left_hip",
    parent=hip_base,
    location=Location(y=LEN_HIP_BASE / 2),
    dimensions=Dimensions(x=LEN_HIP_AA_SIDE),
    ax_rot="x_pos",
    ax_link="y_pos",
    limit=LIM,
)
right_hip = Link(
    name="right_hip",
    parent=hip_base,
    location=Location(y=-LEN_HIP_BASE / 2),
    dimensions=Dimensions(x=LEN_HIP_AA_SIDE),
    ax_rot="x_neg",
    ax_link="y_neg",
    limit=LIM,
)
left_upper_leg = Link(
    name="left_upper_leg",
    parent=left_hip,
    location=Location(x=LEN_HIP_AA_FRONT, y=LEN_HIP_AA_SIDE),
    dimensions=Dimensions(x=LEN_UPPER_LEG),
    ax_rot="y_neg",
    ax_link="z_neg",
    limit=LIM,
)
right_upper_leg = Link(
    name="right_upper_leg",
    parent=right_hip,
    location=Location(x=LEN_HIP_AA_FRONT, y=-LEN_HIP_AA_SIDE),
    dimensions=Dimensions(x=LEN_UPPER_LEG),
    ax_rot="y_neg",
    ax_link="z_neg",
    limit=LIM,
)
left_lower_leg = Link(
    name="left_lower_leg",
    parent=left_upper_leg,
    location=Location(y=-DISPLACE_UPPER_LEG, z=-LEN_UPPER_LEG),
    dimensions=Dimensions(x=LEN_LOWER_LEG),
    ax_rot="y_pos",
    ax_link="z_neg",
    limit=LIM,
)
right_lower_leg = Link(
    name="right_lower_leg",
    parent=right_upper_leg,
    location=Location(y=DISPLACE_UPPER_LEG, z=-LEN_UPPER_LEG),
    dimensions=Dimensions(x=LEN_LOWER_LEG),
    ax_rot="y_pos",
    ax_link="z_neg",
    limit=LIM,
)
left_foot = Link(
    name="left_foot",
    parent=left_lower_leg,
    location=Location(y=-DISPLACE_LOWER_LEG, z=-LEN_LOWER_LEG),
    dimensions=Dimensions(x=LEN_FOOT),
    ax_rot="y_neg",
    ax_link="x_pos",
    limit=LIM,
)
right_foot = Link(
    name="right_foot",
    parent=right_lower_leg,
    location=Location(y=DISPLACE_LOWER_LEG, z=-LEN_LOWER_LEG),
    dimensions=Dimensions(x=LEN_FOOT),
    ax_rot="y_neg",
    ax_link="x_pos",
    limit=LIM,
)

links = [
    left_hip,
    left_upper_leg,
    left_lower_leg,
    left_foot,
    right_hip,
    right_upper_leg,
    right_lower_leg,
    right_foot,
]


def set_view(shading):
    """Set the view to 'WIREFRAME', 'SOLID', 'MATERIAL' or 'RENDERED'."""
    my_areas = bpy.context.workspace.screens[0].areas
    for area in my_areas:
        for space in area.spaces:
            if space.type == "VIEW_3D":
                space.shading.type = shading


def clear_scene():
    """Clear the scene and place cursor at (0, 0, 0)."""
    objects = bpy.data.objects
    for obj in objects.values():
        objects.remove(obj, do_unlink=True)

    collections = bpy.data.collections
    for coll in collections.values():
        collections.remove(coll, do_unlink=True)

    bpy.context.scene.cursor.location = [0, 0, 0]


def create_collections(collections: List[str]):
    for collection in collections:
        collection_object = bpy.data.collections.new(collection)
        bpy.context.scene.collection.children.link(collection_object)


def add_link(link: Link):
    bpy.context.scene.cursor.location = link.loc.as_list
    bpy.ops.phobos.create_links(linkname=link.name)


def set_transformation_matrix(link: Link):
    ax_vectors = {
        "x_pos": [1.0, 0.0, 0.0, 0.0],
        "x_neg": [-1.0, 0.0, 0.0, 0.0],
        "y_pos": [0.0, 1.0, 0.0, 0.0],
        "y_neg": [0.0, -1.0, 0.0, 0.0],
        "z_pos": [0.0, 0.0, 1.0, 0.0],
        "z_neg": [0.0, 0.0, -1.0, 0.0],
    }

    ax_z = ax_vectors[link.ax_rot]
    ax_x = ax_vectors[link.ax_link]
    ax_y = np.append(np.cross(ax_z[:-1], ax_x[:-1]), 0)
    translation = np.append(link.loc_world, 1.0)

    mat = np.column_stack((ax_x, ax_y, ax_z, translation))
    bpy.context.object.matrix_world = Matrix(mat)


def add_object_parent(link: Link, postfix: str = ""):
    """Link an object to its defined parent."""
    objects = bpy.data.objects
    child_object = objects[link.name + postfix]

    if postfix == "":
        parent_object = objects[link.parent.name]
    else:
        parent_object = objects[link.name]

    child_object.parent = parent_object


def add_collision_object_box(link: Link, aligns: List = ["center"]):
    """
    Add a collision object with given dimensions and a given alignment.
    The collision object will first be created with the world origin as origin,
    but after adding a parent, the origin will change to the parent location.
    """
    location = np.array([0.0, 0.0, 0.0])
    for align in aligns:
        if align == "x_neg":
            location += np.array([-link.dim.x / 2, 0.0, 0.0])
            bpy.context.scene.cursor.location = link.loc.as_list
        elif align == "x_pos":
            location += np.array([link.dim.x / 2, 0.0, 0.0])
            bpy.context.scene.cursor.location = link.loc.as_list
        elif align == "y_neg":
            location += np.array([0.0, -link.dim.y / 2, 0.0])
            bpy.context.scene.cursor.location = link.loc.as_list
        elif align == "y_pos":
            location += np.array([0.0, link.dim.y / 2, 0.0])
            bpy.context.scene.cursor.location = link.loc.as_list
        elif align == "z_neg":
            location += np.array([0.0, 0.0, -link.dim.z / 2])
            bpy.context.scene.cursor.location = link.loc.as_list
        elif align == "z_pos":
            location += np.array([0.0, 0.0, link.dim.z / 2])
            bpy.context.scene.cursor.location = link.loc.as_list

    bpy.ops.mesh.primitive_cube_add(
        size=1, scale=link.dim.as_list, location=list(location)
    )
    bpy.ops.phobos.set_phobostype(phobostype="collision")
    bpy.ops.phobos.define_geometry(geomType="box")
    bpy.context.object.name = link.name + "_collision"
    bpy.data.collections['collisions'].objects.link(bpy.context.active_object)
    bpy.context.scene.collection.objects.unlink(bpy.context.active_object) 


def add_object(link: Link, type: str):
    filepath = os.path.join(
        DIRNAME, "stl_files/" + type + "/" + link.name + "_" + type + ".stl"
    )
    bpy.ops.import_mesh.stl(filepath=filepath)
    bpy.ops.phobos.set_phobostype(phobostype=type)
    bpy.ops.phobos.define_geometry(geomType="mesh")
    bpy.context.object.name = link.name + "_" + type
    bpy.data.collections[type+"s"].objects.link(bpy.context.active_object)
    bpy.context.scene.collection.objects.unlink(bpy.context.active_object) 


def add_bone_parent(link: Link, type: str):
    add_object_parent(link, "_" + type)

    bpy.ops.object.select_all(action="DESELECT")
    bpy.data.objects[link.name + "_" + type].select_set(True)
    bpy.data.objects[link.name].select_set(True)
    bpy.context.view_layer.objects.active = bpy.data.objects[link.name]
    bpy.ops.object.parent_set(type="BONE_RELATIVE", keep_transform=True)


def generate_inertia(link: Link):
    print("Calculating inertia for " + link.name + "...")
    bpy.ops.phobos.generate_inertial_objects(
        mass=link.mass, derive_inertia_from_geometry=True, visuals=True
    )
    print("done")


def define_joint(link: Link):
    bpy.ops.phobos.define_joint_constraints(lower=link.lim.lower, upper=link.lim.upper)


def add_model_information(name: str):
    bpy.ops.phobos.name_model(modelname=name)


def export_urdf():
    bpy.context.scene.phobosexportsettings.path = "/home/jelmer/blender/"
    bpy.context.scene.export_entity_urdf = True
    bpy.context.scene.phobosexportsettings.outputMeshtype = "stl"
    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.phobos.export_model()


def main():
    clear_scene()
    set_view("SOLID")
    create_collections(["visuals", "collisions"])

    add_link(hip_base)
    add_collision_object_box(hip_base)
    add_bone_parent(hip_base, "collision")
    add_object(hip_base, "visual")
    add_bone_parent(hip_base, "visual")

    for link in links:
        add_link(link)
        add_object_parent(link)
        set_transformation_matrix(link)
        define_joint(link)
        add_collision_object_box(link, aligns=["x_pos"])
        add_bone_parent(link, "collision")
        add_object(link, "visual")
        add_bone_parent(link, "visual")

    add_model_information(NAME)
    export_urdf()


if __name__ == "__main__":
    main()
